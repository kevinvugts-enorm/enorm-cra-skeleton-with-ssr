const path = require("path");
const webpack = require("webpack");
const { merge } = require("webpack-merge");
const StylelintPlugin = require("stylelint-webpack-plugin");
const ESLintPlugin = require("eslint-webpack-plugin");
const CompressionPlugin = require("compression-webpack-plugin");
const LoadablePlugin = require("@loadable/webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}

const mode = process.env.NODE_ENV;

let config = {
  mode,
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: {
          loader: "babel-loader",
          options: {
            babelrc: true,
          },
        },
        exclude: path.resolve(__dirname, "node_modules/"),
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              esModule: false,
              importLoaders: 2,
            },
          },
          "resolve-url-loader",
          "sass-loader",
          {
            loader: "postcss-loader",
            options: {
              postcssOptions: {
                plugins: ["postcss-preset-env"],
              },
            },
          },
        ],
      },
      {
        test: /fonts\/[^.]+\.(ttf|eot|svg|woff(2)?)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "file-loader",
        options: {
          outputPath: "static/fonts",
        },
      },
      {
        test: /\.(svg|png|jpg|jpeg|gif)$/,
        loader: "file-loader",
        options: {
          outputPath: "static/images",
        },
      },
    ],
  },
  resolve: {
    extensions: [".js", ".jsx", ".tsx", ".ts", ".css", ".scss"],
    fallback: {
      buffer: require.resolve("buffer/"), // Webpack 5 polyfill used by React HTML parser
    },
  },
  plugins: [
    new LoadablePlugin(),
    new webpack.DefinePlugin({
      APP_CONFIG: JSON.stringify({
        apiHost: process.env.API_HOST || "http://localhost:1337",
        apiVersion: process.env.API_VERSION,
        googleMapsApiKey: process.env.GOOGLE_MAPS_API_KEY,
        googleTagManagerId: process.env.GOOGLE_TAGMANAGER_ID,
        imageKitUrl:
          process.env.IMAGE_KIT_URL ||
          "https://ik.imagekit.io/dutchkiwidesign/apk-station-goirle-dev",
        sentryDns: process.env.SENTRY_DNS,
      }),
    }),
    new StylelintPlugin({
      files: "./src/shared/assets/scss/**/*.scss",
    }),
    new ESLintPlugin(),
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css",
    }),
  ],
};

if (mode === "development") {
  config = merge(config, {
    // devtool: "eval-source-map", // Disabled to make @font-face work in css in development mode
    stats: "errors-only",
  });
}

if (mode === "production") {
  config = merge(config, {
    cache: true,
    stats: {
      colors: true,
    },
  });
  config.plugins.push(
    new CompressionPlugin({
      filename: "[path][base].gz",
      algorithm: "gzip",
      test: /\.js$|\.jsx$|\.css$|\.html$/,
      minRatio: 0.8,
    })
  );
}

module.exports = config;
