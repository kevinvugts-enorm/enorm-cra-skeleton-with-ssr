const path = require("path");
const nodeExternals = require("webpack-node-externals");
const { merge } = require("webpack-merge");
const common = require("./webpack.common");

let config = merge(common, {
  entry: ["core-js", path.resolve(__dirname, "../src")],
  target: "node",
  node: {
    __dirname: false,
    __filename: false,
  },
  externals: [nodeExternals()],
  output: {
    path: path.resolve(__dirname, "../build"),
    filename: "server.js"
  },
});

module.exports = config;
