const path = require("path");
const webpack = require("webpack");
const { merge } = require("webpack-merge");
const common = require("./webpack.common");

let config = merge(common, {
  target: "web",
  entry: ["core-js", path.resolve(__dirname, "../src/client/components")],
  output: {
    path: path.resolve(__dirname, "../build/web"),
    publicPath: "/web/",
    filename: "[name].js",
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendor",
          chunks: "all",
          enforce: true,
        },
      },
    },
    runtimeChunk: {
      name: "manifest",
    },
  },
});

if (process.env.NODE_ENV === "development") {
  config = merge(config, {
    entry: [
      "webpack-hot-middleware/client?path=http://localhost:3001/__webpack_hmr",
    ],
  });
  config.plugins.push(new webpack.HotModuleReplacementPlugin());
}

module.exports = config;
