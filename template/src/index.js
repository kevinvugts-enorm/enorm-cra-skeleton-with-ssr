// Dependencies
import express from "express";
import fs from "fs";
// Client
import clientApplication from "./client";
// Server
import serverApplication from "./server";

const path = require("path");

const PORT = process.env.PORT || 3001;

let app = express();

app.use(clientApplication);

if (process.env.NODE_ENV === "development") {
  app.use((req, res, next) => {
    import("./server").then((mod) => mod.default(req, res, next));
  });
} else {
  app.use(serverApplication);
}

app.on("listening", () => {
  console.log(`Environment -> ${process.env.NODE_ENV}`);
  console.log(`Listening   -> http://localhost:${PORT}/`);
});

if (module.hot && process.env.NODE_ENV === "development") {
  const walkSync = (dir, filelist = []) => {
    const files = fs.readdirSync(dir);

    filelist = filelist || [];
    files.forEach(function (file) {
      if (fs.statSync(path.join(dir, file)).isDirectory()) {
        filelist = walkSync(path.join(dir, file), filelist);
      } else {
        filelist.push(path.join(dir, file));
      }
    });
    return filelist;
  };

  module.hot.accept("./server");
  module.hot.decline(
    walkSync(path.resolve(__dirname, "../src")).filter((filePath) =>
      /client/.test(filePath)
    )
  );
}

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
