// Dependencies
const express = require("express");
const app = express();
const cors = require("cors");
const path = require("path");
const compression = require("compression");
// Render
import renderServerSideApp from "./render";

function shouldCompress(req, res) {
  if (req.headers["x-no-compression"]) {
    return false;
  }
  return compression.filter(req, res);
}

function setNoCache(res) {
  const date = new Date();
  date.setFullYear(date.getFullYear() - 1);
  res.setHeader("Expires", date.toUTCString());
  res.setHeader("Pragma", "no-cache");
  res.setHeader("Cache-Control", "public, no-cache");
}

function setLongTermCache(res) {
  const date = new Date();
  date.setFullYear(date.getFullYear() + 1);
  res.setHeader("Expires", date.toUTCString());
  res.setHeader("Cache-Control", "public, max-age=31536000, immutable");
}

app.use(
  compression({
    threshold: 0,
    filter: shouldCompress,
  })
);

app.use(
  cors({
    origin: "*",
  })
);

// For some odd reason favicon is being called on the server
app.get("/favicon.ico", function (req, res) {
  res.sendStatus(204);
});

app.use(
  express.static(path.join(__dirname), {
    extensions: ["html"],
    setHeaders(res, path) {
      if (path.match(/(\.html|\/sw\.js)$/)) {
        setNoCache(res);
        return;
      }

      if (path.match(/\.(js|css|png|jpg|jpeg|gif|ico|json|woff|woff2)$/)) {
        setLongTermCache(res);
      }
    },
  })
);

// Routes
renderServerSideApp(app);

export default app;
