// Dependencies
import React from "react";
import ReactDOMServer from "react-dom/server"; // allows us to render react components tree on the server

// Loadable components
import { ChunkExtractor } from "@loadable/server";

const path = require("path");

import { StaticRouter } from "react-router-dom";
import { Helmet } from "react-helmet";
import axios from "axios";
import { QueryClientProvider } from "react-query";

import { dehydrate, Hydrate } from "react-query/hydration";

// Utils
import {
  generateErrorMessageArray,
  generateErrorMessageArrayForPost,
} from "../shared/utils/strapi";

import { queryClientServer } from "../../config/caching";

// App
import App from "../shared/App"; // our component which will be rendered on the server

const renderServerSideApp = async (app) => {
  app.get("*", async (req, res) => {
    let slug = /[^/]*$/.exec(req.url)[0]; // get everything after the last slash as it's the slug of the page

    if (slug === undefined || slug === "") {
      slug = "home";
    }

    // React loadable setup
    const webStats = path.resolve(__dirname + "/web/loadable-stats.json");

    // We create an extractor from the statsFile
    const webExtractor = new ChunkExtractor({ statsFile: webStats });

    await queryClientServer.prefetchQuery(["pages", slug], () =>
      axios
        .get(`${APP_CONFIG.apiHost}/pages?slug=${slug}`)
        .then((res) => {
          if (Array.isArray(res.data) && res.data.length === 0) {
            return Promise.reject({
              status: 404,
              message: "Pagina niet gevonden",
            });
          }

          return res.data;
        })
        .catch((error) => {
          if (!error.response && !error.message) {
            return Promise.reject({ message: "Er is een fout opgetreden" });
          } else if (error.message && Array.isArray(error.message)) {
            return Promise.reject(generateErrorMessageArray(error));
          } else if (error.data && error.data.errors) {
            return Promise.reject(generateErrorMessageArrayForPost(error.data));
          } else {
            return Promise.reject(error.response ? error.response.data : error);
          }
        })
    );
    const dehydratedState = dehydrate(queryClientServer);

    // renderToStaticMarkup omits all the HTML attributes React adds to the DOM during rendering
    const appMarkup = ReactDOMServer.renderToString(
      webExtractor.collectChunks(
        <StaticRouter location={req.url} context={{}}>
          <QueryClientProvider client={queryClientServer}>
            <Hydrate state={dehydratedState}>
              <App initialData={dehydratedState} />
            </Hydrate>
          </QueryClientProvider>
        </StaticRouter>
      )
    );

    const scriptTags = webExtractor.getScriptTags(); // or extractor.getScriptElements();
    const linkTags = webExtractor.getLinkTags(); // or extractor.getLinkElements();
    const styleTags = webExtractor.getStyleTags();

    const helmet = Helmet.renderStatic();

    res.set("content-type", "text/html");

    res
      .status(200)
      .send(
        renderFullPage(
          appMarkup,
          helmet,
          dehydratedState,
          styleTags,
          scriptTags,
          linkTags
        )
      );
  });
};

const renderFullPage = (
  html,
  helmet,
  initialData,
  loadableStyleTags,
  loadableScriptTags,
  loadableLinkTags
) => {
  return `
  <!DOCTYPE html>
  <html ${helmet.htmlAttributes.toString()}>
    <head>
      <meta charSet="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />

      ${helmet.title.toString()}
      ${helmet.meta.toString()}

      <link rel="preconnect" href="https://kit-pro.fontawesome.com" crossorigin="anonymous" />
      <script async defer src="https://kit.fontawesome.com/fc7124aa78.js" crossorigin="anonymous"></script>

      <link rel="preconnect" href="https://fonts.gstatic.com" />
      <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;700&family=Roboto:wght@500&display=swap" rel="stylesheet" />

      ${loadableLinkTags}
      ${loadableStyleTags}

    </head>
    <body ${helmet.bodyAttributes.toString()}>
      <div id="app">${html}</div>
      ${
        initialData &&
        `<script>
            window.__REACT_QUERY_STATE__ = ${JSON.stringify(initialData)}
        </script>`
      }
      ${loadableScriptTags}

      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script async defer src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
      <script async defer src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

    </body>
  </html>`;
};

export default renderServerSideApp;
