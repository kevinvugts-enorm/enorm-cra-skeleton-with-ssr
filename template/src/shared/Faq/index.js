// Dependencies
import React from "react";
import ReactHtmlParser from "react-html-parser";

// Components
import FaqCategories from "./Categories";
import FaqQuestions from "./Questions";

// REFACTOR COMPONENT BASED ON KENT'S: https://codesandbox.io/s/simply-react-accordion-forked-35yey?file=/src/index.js
const FaqBlock = (instanceProps) => {
  const [activeId, setActiveId] = React.useState("");
  const [activeCategory, setActiveCategory] = React.useState("Montage"); // SHOULD NOT BE HARDCODED / BASED ON ID

  const { title, faq_categories } = instanceProps;

  return (
    <section className="section">
      <div className="faq">
        <div className="container">
          {/* FAQ Content Title */}
          <div className="faq__title">
            <div className="row">
              <div className="col-12">{ReactHtmlParser(title)}</div>
            </div>
          </div>

          <div className="faq__content">
            <div className="row">
              {/* FAQ Category Buttons */}
              <div className="col-sm-3">
                <FaqCategories
                  activeCategory={activeCategory}
                  categories={faq_categories}
                  setActiveCategory={setActiveCategory}
                />
              </div>

              {/* FAQ Question Collapsables */}
              <div className="col-sm-9 mt-4 mt-sm-0">
                <FaqQuestions
                  activeCategory={activeCategory}
                  activeId={activeId}
                  categories={faq_categories}
                  setActiveId={setActiveId}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default FaqBlock;
