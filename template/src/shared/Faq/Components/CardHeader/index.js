import React from "react";
import ReactHtmlParser from "react-html-parser";

const CardHeader = ({ id, question, isActive, setActiveId }) => {
  // We need to keep references to the svg, as react otherwise cannot interact via the virtual dom...
  // TODO: consider moving away from svg since we need to be able to interact with it.
  // When already active it swaps again
  const openedIcon = (
    <i style={{ fontSize: 30 }} className="ml-1 fas fa-caret-up"></i>
  );
  const closedIcon = (
    <i style={{ fontSize: 30 }} className="ml-1 fas fa-caret-down"></i>
  );

  return (
    <div
      className="faq__question-item-header"
      id={`heading-${id}`}
      role="button"
      data-toggle="collapse"
      data-target={`#collapse-${id}`}
      aria-expanded="true"
      aria-controls={`collapse-${id}`}
      onClick={() => setActiveId(id)}
    >
      <div className="faq__question-itemheader__title">
        {ReactHtmlParser(question.question)}
      </div>

      <div className="faq__question-itemheader__icon">
        <span className={`${isActive ? "d-block" : "d-none"}`}>
          {openedIcon}
        </span>

        <span className={`${!isActive ? "d-block" : "d-none"}`}>
          {closedIcon}
        </span>
      </div>
    </div>
  );
};

export default CardHeader;
