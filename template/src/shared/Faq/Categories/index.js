import React from "react";

const FaqCategories = ({ categories, activeCategory, setActiveCategory }) => (
  <div className="faq__categories">
    {categories.map(({ title }, key) => (
      <button
        key={key}
        className={`btn btn-block btn-outline-theme-secondary ${
          activeCategory === title ? "active" : ""
        }`}
        type="button"
        onClick={() => setActiveCategory(title)}
      >
        {title}
      </button>
    ))}
  </div>
);

export default FaqCategories;
