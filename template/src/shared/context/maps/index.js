import React from "react";

const ActiveMarkerContext = React.createContext();
ActiveMarkerContext.displayName = "ActiveMarkerContext";

function ActiveMarkerProvider(props) {
  const [activeMarkerId, setActiveMarkerId] = React.useState(
    props.initialState || null
  );

  return (
    <ActiveMarkerContext.Provider
      value={{ activeMarkerId, setActiveMarkerId }}
      {...props}
    />
  );
}

function useGoogleMapsActiveMarker() {
  const context = React.useContext(ActiveMarkerContext);
  if (context === undefined) {
    throw new Error(
      `useGoogleMapsActiveMarker must be used within a ActiveMarkerProvider`
    );
  }
  return context;
}

export { ActiveMarkerProvider, useGoogleMapsActiveMarker };
