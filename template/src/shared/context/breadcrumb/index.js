import React from "react";

const BreadcrumbContext = React.createContext();
BreadcrumbContext.displayName = "BreadcrumbContext";

// Generates an array of active breadcrumbs based on the routers routes coming from strapi
function BreadCrumbProvider(props) {
  function generateCrumbs() {
    const crumbs = props.routes
      // Get all routes that contain the current one.
      .filter(({ path }) => props.match.path.includes(path))
      // Swap out any dynamic routes with their param values.
      .map(({ path, ...rest }) => ({
        path: Object.keys(props.match.params).length
          ? Object.keys(props.match.params).reduce(
              (path, param) =>
                path.replace(`:${param}`, props.match.params[param]),
              path
            )
          : path,
        ...rest,
      }));

    return crumbs;
  }

  return (
    <BreadcrumbContext.Provider
      value={{ breadcrumbs: generateCrumbs() }}
      {...props}
    />
  );
}

function useBreadCrumbs() {
  const context = React.useContext(BreadcrumbContext);
  if (context === undefined) {
    throw new Error(`useBreadCrumbs must be used within a BreadCrumbProvider`);
  }
  return context;
}

export { BreadCrumbProvider, useBreadCrumbs };
