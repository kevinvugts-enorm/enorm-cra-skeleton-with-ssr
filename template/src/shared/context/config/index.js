import React from "react";
//import { queryCache } from "react-query";
// import * as auth from "../../auth-provider";
// import { client } from "@utils/api-client";
import { useAsync } from "../../hooks";
import { sequentialPromises } from "../../utils";

//import { FullPageSpinner, FullPageErrorFallback } from "@components/lib";
import axios from "axios";

// Components
import DefaultPage from "../../pages/Default";
import Loading from "../../elements/Loading";

const StrapiContext = React.createContext();
StrapiContext.displayName = "StrapiContext";

// List of components which will reflect the choices in Strapi (Breadcrumbs -> Template (enum))
const COMPONENTS = {
  defaultPage: DefaultPage,
};

function StrapiConfigProvider(props) {
  const {
    data,
    status,
    isLoading,
    isIdle,
    isError,
    isSuccess,
    run,
  } = useAsync();

  // Translates the route.componentName to a React component which will be rendered by React Router
  function getReactComponentFromName(name) {
    const Component = COMPONENTS[name];

    if (Component) {
      return Component;
    } else {
      throw new Error(`Unsupported Component type ${name}`);
    }
  }

  // Mutates the routes array comming from Strapi and attach a renderable component to it
  const transformRoutesComponentName = React.useCallback((data) => {
    return data.routes.map((route) => {
      return {
        Component: getReactComponentFromName(data.template),
        slug: "home",
        ...route,
      };
    });
  }, []);

  React.useEffect(() => {
    async function loadConfig() {
      let routesPromise = axios.get(`${APP_CONFIG.apiHost}/routes`);
      let footerPromise = axios.get(`${APP_CONFIG.apiHost}/global`);

      // Currently we have no easy way to run multiple requests with our runner function
      run(
        new Promise((resolve, reject) =>
          sequentialPromises([routesPromise, footerPromise]).then((values) => {
            const routes = values[0].data;
            const footer = values[1].data;
            resolve({ routes: transformRoutesComponentName(routes), footer });
          })
        )
      );
    }

    loadConfig();
  }, [run, transformRoutesComponentName]);

  const value = React.useMemo(
    () => ({
      routes: data ? data.routes : [],
      footer: data ? data.footer : {},
    }),
    [data]
  );

  if (isIdle || isLoading) {
    return <Loading />;
  }

  if (isError) {
    return <p>error page</p>;
  }

  if (isSuccess) {
    return <StrapiContext.Provider value={value} {...props} />;
  }

  throw new Error(`Unhandled status: ${status}`);
}

function useStrapiConfig() {
  const context = React.useContext(StrapiContext);
  if (context === undefined) {
    throw new Error(`useAuth must be used within a AuthProvider`);
  }
  return context;
}

export { StrapiConfigProvider, useStrapiConfig };
