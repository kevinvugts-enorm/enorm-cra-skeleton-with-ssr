// Strapi
import { generateErrorMessageArray, getImageURL } from "./strapi";
// Api client
import { client } from "./api-client";

const sequentialPromises = (tasks) => {
  return tasks.reduce((promiseChain, currentTask) => {
    return promiseChain.then((chainResults) =>
      currentTask.then((currentResult) => [...chainResults, currentResult])
    );
  }, Promise.resolve([]));
};

export { generateErrorMessageArray, getImageURL, client, sequentialPromises };
