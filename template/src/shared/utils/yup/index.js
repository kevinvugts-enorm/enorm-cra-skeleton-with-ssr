/* ==========================================================================
Yup
https://github.com/jquense/yup
========================================================================== */
import * as Yup from "yup";

export function createYupSchema(schema, config) {
  const { name, validationType, validation = [] } = config;
  if (!Yup[validationType]) {
    return schema;
  }
  let validator = Yup[validationType]();
  validation.forEach((validation) => {
    const { params, type } = validation;
    if (!validator[type]) {
      return;
    }
    validator = validator[type](...params);
  });
  schema[name] = validator;
  return schema;
}
