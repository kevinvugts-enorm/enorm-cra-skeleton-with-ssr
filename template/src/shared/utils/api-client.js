// Dependencies
import { queryCache } from "react-query";
// Strapi
import {
  generateErrorMessageArray,
  generateErrorMessageArrayForPost,
} from "./strapi";

async function client(
  endpoint,
  { data, token, headers: customHeaders, ...customConfig } = {}
) {
  const config = {
    method: data ? "POST" : "GET",
    body: data ? JSON.stringify(data) : undefined,
    headers: {
      "Content-Type": data ? "application/json" : undefined,
      ...customHeaders,
    },
    ...customConfig,
  };

  if (token) {
    config.headers = {
      ...config.headers,
      Authorization: `Bearer ${token}`,
    };
  }

  return (
    window
      .fetch(`${APP_CONFIG.apiHost}/${endpoint}`, config)
      .then(async (response) => {
        if (response.status === 401) {
          queryCache.clear();
          // refresh the page for them
          window.location.assign(window.location);
          return Promise.reject({ message: "Please re-authenticate." });
        }

        if (response.ok) {
          const data = await response.json();

          return data;
        } else {
          let message = "";

          switch (response.status) {
            case 403:
              message = "Je bent niet bevoegd om deze pagina te bekijken";
              break;
            case 404:
              message = "Oeps, we hebben de pagina niet kunnen vinden.";
              break;
            default:
              message =
                "Er is een fout opgetreden tijdens het laden van de pagina";
              break;
          }

          return Promise.reject({ message, status: response.status });
        }
      })
      // STRAPI IS ENDING DIFFERENT ERROR RESPONSE WE NEED TO HANDLE IT BETTER
      .catch((error) => {
        if (error.message && error.message === "Failed to fetch") {
          return Promise.reject({
            message: "Kon de api niet bereiken",
            status: 500,
          });
        } else if (!error.response && !error.message) {
          return Promise.reject({ message: "Er is een fout opgetreden" });
        } else if (error.message && Array.isArray(error.message)) {
          return Promise.reject(generateErrorMessageArray(error));
        } else if (error.data && error.data.errors) {
          return Promise.reject(generateErrorMessageArrayForPost(error.data));
        } else {
          return Promise.reject(error.response ? error.response.data : error);
        }
      })
  );
}

export { client };
