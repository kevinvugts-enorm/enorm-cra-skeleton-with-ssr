// Dependencies
import React from "react";
import Router from "./core/Router";
import { IKContext } from "imagekitio-react";
import * as Sentry from "@sentry/react";

// context
import { StrapiConfigProvider } from "./context/config";

// Seed
import { errorDialogOptions } from "./seed";

// Stylesheet
import "./assets/scss/main.scss";

// Variables
const urlEndpoint = APP_CONFIG.imageKitUrl;

Sentry.init({
  dsn: APP_CONFIG.sentryDns,
});

const App = () => {
  return (
    <Sentry.ErrorBoundary
      showDialog
      dialogOptions={errorDialogOptions}
      fallback={() => <p>Fallback page here...</p>}
    >
      <StrapiConfigProvider>
        <IKContext urlEndpoint={urlEndpoint}>
          <Router />
        </IKContext>
      </StrapiConfigProvider>
    </Sentry.ErrorBoundary>
  );
};

export default App;
