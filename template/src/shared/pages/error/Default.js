// Dependencies
import React from "react";
// Core
import Head from "../../core/Head";
// Blocks
import BlockHero from "../../blocks/Hero";
// import BlockHero from "../../blocks/Hero/Homepage";
import BlockFeature from "../../blocks/Feature";

export default (props) => {
  return (
    <div className={`page`}>
      <Head
        pageTitle="404 - Pagina niet gevonden"
        seo={[
          { meta_description: "", meta_title: "", __component: "seo.meta" },
          { og_image: "", og_title: "", __component: "seo.open-graph" },
        ]}
        {...props}
      />

      <BlockHero
        title=":'("
        content="### Oeps, we hebben niets kunnen vinden."
        background_image={[
          {
            url: "/assets/img/hero_404_dbe74b7899.jpg",
          },
        ]}
        key="error-hero-404"
        slug="404"
        link={{
          url: "/collections",
          title: "Get inspired",
          className: "btn btn-primary",
        }}
      />

      <BlockFeature />
    </div>
  );
};
