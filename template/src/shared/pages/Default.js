// Dependencies
import React from "react";
import { useQuery } from "react-query";

// Core
import Head from "../core/Head";
import Area from "../core/Area";

// Utils
import { client } from "../utils/api-client";

// Elements
import Loading from "../elements/Loading";

// Error
import ErrorPage from "./error/Default";

export default (props) => {
  if (props.slug === undefined || props.slug === "") {
    props.slug = "home";
  }

  // TODO: Handle in hooks
  const { data, isLoading, error, isError } = useQuery(
    ["pages", props.slug],
    () => client(`pages?slug=${props.slug}`)
  );

  if (isLoading) {
    return <Loading {...props} />;
  }

  if (isError) {
    switch (error.status) {
      case 403:
      case 404:
      case 500:
      default:
        return <ErrorPage {...props} />;
    }
  }

  const page = data && data[0];
  const seo = page && page["SEO"];

  return (
    <main className="page">
      <Head seo={seo} pageTitle={page.slug || "Default"} />
      <Area blocks={page.content} slug={page.slug} />
    </main>
  );
};
