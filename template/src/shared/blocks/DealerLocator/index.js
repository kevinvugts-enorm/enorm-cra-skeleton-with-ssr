// Dependencies
import React from "react";
import ReactHtmlParser from "react-html-parser";
import { GoogleApiWrapper } from "google-maps-react";

// Services
import { DealerService, GoogleMapsService } from "../../services";

// Hook
import { useAsync } from "../../hooks";

// Components
import MapContainer from "../../components/Maps/MapContainer";
import AutoComplete from "../../components/Maps/AutoComplete";
import LocationList from "../../components/Maps/LocationList";

// Services
const dealer = new DealerService();
const googleMaps = new GoogleMapsService();

import { ActiveMarkerProvider } from "../../context/maps";

const DealerLocator = (props) => {
  const [latLng, setLatLng] = React.useState(null);
  const state = useAsync({ status: latLng ? "pending" : "idle" });
  const { run, data: markers, status, error } = state;

  // Get dealer data
  React.useEffect(() => {
    run(googleMaps.getMarkers(latLng, dealer.get().response.data));
  }, [latLng, run]);

  return (
    /* Wrapper dealer list block */
    <ActiveMarkerProvider>
      <section className="section">
        <div className="container">
          {/* Content component inside dealer locator block */}
          {props.title && ReactHtmlParser(props.title)}
          <div className="row dealer__locator">
            <div className="dealer__map">
              {!error ? (
                <MapContainer {...props} markers={markers} />
              ) : (
                <div class="alert alert-danger" role="alert">
                  {error}
                </div>
              )}
            </div>
            <aside className="dealer__sidebar">
              <AutoComplete
                setLocation={(latLng) => setLatLng(latLng)}
                resetMarkers={() => setLatLng(null)}
              />
              <h5 className="dealer__sidebar-title">
                Dichtstbijzijnde montagelocaties
              </h5>
              {<LocationList status={status} dealers={markers} />}
            </aside>
          </div>
        </div>
      </section>
    </ActiveMarkerProvider>
  );
};

export default GoogleApiWrapper({
  apiKey: APP_CONFIG.googleMapsApiKey,
  language: "nl",
})(DealerLocator);
