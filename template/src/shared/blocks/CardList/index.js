// Dependencies
import React from "react";
import ReactHtmlParser from "react-html-parser";
import PropTypes from "prop-types";

// Components
import Card from "../../components/Card";

const CardList = ({ mode, cards, title }) => {
  const renderCards = () =>
    cards.map((item, key) => {
      return <Card key={key} {...item} />;
    });

  return (
    /* Wrapper card list block */
    <section className={`section card__list card__list--mode-${mode}`}>
      <div className="container">
        {/* Content component inside card list block */}
        <div className="card__list-title">{ReactHtmlParser(title)}</div>
        <div className="row">
          {/* Repeatable card components inside card list block */}
          {renderCards()}
        </div>
      </div>
    </section>
  );
};

CardList.propTypes = {
  mode: PropTypes.oneOf(["dark", "light"]),
  cards: PropTypes.array,
  title: PropTypes.string,
};

export default CardList;
