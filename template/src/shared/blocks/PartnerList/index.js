import React from "react";
import ReactHtmlParser from "react-html-parser";
import PropTypes from "prop-types";

// Swiper
import SwiperCore, { Mousewheel } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";

SwiperCore.use([Mousewheel]);

import { useSwiperSlides } from "../../hooks";

const PartnerList = ({ mode, content, partners }) => {
  const slidesPerView = useSwiperSlides({
    xs: 1,
    sm: 3,
    md: 3,
    lg: 5,
    xl: 5,
    xxl: 5,
  }); // calculates the amount of slides to show per device

  return (
    <section className={`section section--mode-${mode} partner__list`}>
      <div className="container">
        <div className="partner__list-title">{ReactHtmlParser(content)}</div>
        <div className="row">
          <div className="col">
            <Swiper
              spaceBetween={30}
              freeMode={true}
              grabCursor={true}
              mousewheel={{
                forceToAxis: true,
              }}
              watchOverflow={true}
              watchSlidesVisibility={true}
              slidesPerView={slidesPerView}
            >
              {partners.map((partner, key) => (
                <SwiperSlide className="partner__list-item" key={key}>
                  <img src={partner.logo.url} alt={partner.logo.name} />
                </SwiperSlide>
              ))}
            </Swiper>
          </div>
        </div>
      </div>
    </section>
  );
};

PartnerList.propTypes = {
  content: PropTypes.string.isRequired,
  partners: PropTypes.array,
  mode: PropTypes.oneOf(["dark", "light"]),
};

export default PartnerList;
