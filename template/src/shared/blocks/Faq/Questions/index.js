import React from "react";

// Components
import { CardHeader, CardContent } from "../Components";

const FaqCategories = ({
  categories,
  activeCategory,
  activeId,
  setActiveId,
}) => (
  <div className="faq__questions" id="accordion">
    {categories
      .find((e) => e.title === activeCategory)
      .faq_questions.map(({ id, ...question }, key) => (
        <div className="faq__question-item" key={key}>
          <CardHeader
            question={question}
            className="faq__question-header"
            id={id}
            setActiveId={setActiveId}
            isActive={activeId === id}
          />
          <CardContent
            question={question}
            classname="faq__question-content"
            id={id}
          />
          <hr />
        </div>
      ))}
  </div>
);

export default FaqCategories;
