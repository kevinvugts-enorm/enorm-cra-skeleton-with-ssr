import React from "react";
import ReactHtmlParser from "react-html-parser";

const CardContent = ({ id, question }) => {
  return (
    <div
      id={`collapse-${id}`}
      className="collapse"
      aria-labelledby={`heading-${id}`}
      data-parent="#accordion"
    >
      <div>{ReactHtmlParser(question.answer)}</div>
    </div>
  );
};

export default CardContent;
