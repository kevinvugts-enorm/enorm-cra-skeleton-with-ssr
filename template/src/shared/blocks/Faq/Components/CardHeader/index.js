import React from "react";
import ReactHtmlParser from "react-html-parser";

const CardHeader = ({ id, question, isActive, setActiveId }) => {
  return (
    <div
      className="faq__question-item-header"
      id={`heading-${id}`}
      role="button"
      data-toggle="collapse"
      data-target={`#collapse-${id}`}
      aria-expanded="false"
      aria-controls={`collapse-${id}`}
      onClick={() => setActiveId(id)}
    >
      <div className="faq__question-item-header__title">
        {ReactHtmlParser(question.question)}
      </div>

      <div className="faq__question-item-header__icon">
        <i className="fas fa-caret-up"></i>
      </div>
    </div>
  );
};

export default CardHeader;
