// Dependencies
import React from "react";
import PropTypes from "prop-types";
import ReactHtmlParser from "react-html-parser";

// Component
import Button from "../../components/Button";

function underscoreReplace(string) {
  let newString = string.replaceAll("_", "-");

  return newString;
}

const ThanksBlock = ({ buttons, content, mode }) => {
  return (
    <section className={`section thankyou thankyou--mode-${mode}`}>
      <div className="container-fluid">
        <div className="row align-items-center justify-content-center">
          <div className="col-12 col-md-6 text-center">
            <div className="thankyou__content">{ReactHtmlParser(content)}</div>

            <div className="thankyou_actions">
              {buttons.map((button, key) => {
                return (
                  <Button
                    key={key}
                    theme={`${underscoreReplace(button.theme)}`}
                    type={button.type_button}
                    title={button.title}
                  />
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

ThanksBlock.propTypes = {
  mode: PropTypes.oneOf(["dark", "light"]),
  content: PropTypes.string,
  buttons: PropTypes.array,
};

export default ThanksBlock;
