// Dependencies
import React from "react";
import PropTypes from "prop-types";
import ReactHtmlParser from "react-html-parser";
// Assets
import featurePlaceholder from "../../assets/images/hero-placeholder.jpg";
// Component
import Button from "../../components/Button";

const Feature = ({ alignment, background_image, content, buttons, mode }) => {
  return (
    <div
      className={`feature feature--align-${alignment} feature--mode-${mode} feature--height-xxl`}
    >
      <div className="container-fluid">
        <div className="row align-items-center justify-content-end no-gutters">
          <div className="col-4 col-image">
            <div className="feature__image">
              <img
                className="img-fluid"
                src={
                  (background_image && background_image.url) ||
                  featurePlaceholder
                }
                alt={
                  (background_image && background_image.name) ||
                  "Feature Placeholder"
                }
              />
              <div className="feature__mask">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 220 750"
                  preserveAspectRatio="none"
                >
                  <path
                    d="M0,750V0H220L15,325S5,340,5,375s10,50,10,50L220,750Z"
                    fill="#0f1223"
                  />
                </svg>
              </div>
              {/* .feature__mask */}
            </div>
            {/* .feature__image */}
          </div>
          {/* .col */}
          <div className="col-sm-8 col-text">
            <div className="feature__text feature--text-large">
              {ReactHtmlParser(content)}
              {buttons && (
                <div className="btn-group">
                  {buttons.map((button, key) => {
                    return <Button key={key} {...button} />;
                  })}
                </div>
              )}
            </div>
          </div>
          {/* .col */}
        </div>
        {/* .row */}
      </div>
      {/* .container-fluid */}
    </div>
  );
};

Feature.propTypes = {
  mode: PropTypes.oneOf(["dark", "light"]),
  content: PropTypes.string,
  // background_image: PropTypes.string,
  alignment: PropTypes.oneOf(["left", "right"]),
};

export default Feature;
