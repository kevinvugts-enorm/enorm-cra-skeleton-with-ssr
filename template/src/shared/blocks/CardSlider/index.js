// Dependencies
import React from "react";
import ReactHtmlParser from "react-html-parser";

import SwiperCore, { Pagination, Mousewheel } from "swiper";

import { Swiper, SwiperSlide } from "swiper/react";
import PropTypes from "prop-types";

// Components
import Card from "../../components/Card";

SwiperCore.use([Pagination, Mousewheel]);

import { useSwiperSlides } from "../../hooks";

const CardSlider = ({ mode, cards, title }) => {
  const slidesPerView = useSwiperSlides({
    xs: 1,
    sm: 1,
    md: 2,
    lg: 3,
    xl: 3,
    xxl: 3,
  }); // calculates the amount of slides to show per device

  const renderCards = () =>
    cards.map((item, key) => {
      return (
        <SwiperSlide key={key} className="card__slider-item">
          <Card {...item} />
        </SwiperSlide>
      );
    });

  return (
    /* Wrapper card list block */
    <section className={`section card__slider card__slider--mode-${mode}`}>
      <div className="container">
        {/* Content component inside card list block */}
        <div className="card__slider-title">{ReactHtmlParser(title)}</div>
        <div className="row">
          <div className="col">
            <Swiper
              freeMode={true}
              grabCursor={true}
              mousewheel={{
                forceToAxis: true,
              }}
              pagination={{
                el: ".swiper-pagination",
                dynamicBullets: true,
              }}
              watchOverflow={true}
              watchSlidesVisibility={true}
              slidesPerView={slidesPerView}
              breakpoints={{
                0: {
                  spaceBetween: 30,
                },
                992: {
                  spaceBetween: 60,
                },
              }}
            >
              {/* Repeatable card components inside card slider block */}
              {renderCards()}
              <div className="swiper-pagination"></div>
            </Swiper>
          </div>
        </div>
      </div>
    </section>
  );
};

CardSlider.propTypes = {
  mode: PropTypes.oneOf(["dark", "light"]),
  cards: PropTypes.array,
  title: PropTypes.string,
};

export default CardSlider;
