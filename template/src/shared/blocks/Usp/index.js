import React from "react";
import PropTypes from "prop-types";

// Swiper
import SwiperCore, { Mousewheel, Autoplay } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";

SwiperCore.use([Mousewheel, Autoplay]);

import { useSwiperSlides } from "../../hooks";

const UspBlock = ({ mode, usps }) => {
  const slidesPerView = useSwiperSlides(); // calculates the amount of slides to show per device

  return (
    <section className={`section section--no-padding section--mode-${mode}`}>
      <div className="container">
        <div className="row">
          <Swiper
            className={`usps-slider usps-slider--mode-${mode}`}
            freeMode={true}
            grabCursor={true}
            mousewheel={{
              forceToAxis: true,
            }}
            slidesPerView={slidesPerView}
            spaceBetween={50}
            autoplay={{
              delay: 5000,
            }}
          >
            {usps.map((usp, key) => (
              <SwiperSlide
                className="usps-slider__item"
                key={key}
                role="listitem"
              >
                <img
                  alt={usp.title}
                  className="usps-slider__icon"
                  height={20}
                  src={usp.icon.url}
                  width={20}
                />
                <h5 className="usps-slider__title">{usp.title}</h5>
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
      </div>
    </section>
  );
};

UspBlock.propTypes = {
  mode: PropTypes.oneOf(["dark", "light"]),
  usps: PropTypes.array,
};

export default UspBlock;
