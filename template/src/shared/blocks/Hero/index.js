// Dependencies
import React from "react";
import ReactHtmlParser from "react-html-parser";
import PropTypes from "prop-types";
// Assets
import heroLogo from "../../assets/images/hero-logo.svg";
import heroSvg from "../../assets/images/hero-svg.svg";
// Components
import LicensePlateWidget from "../../components/Widget";
import Button from "../../components/Button";

const Hero = ({ content, mode, title, widget, buttons }) => {
  return (
    <div className={`hero hero--mode-${mode} hero--height`}>
      <div className="hero__block">
        <div className="hero__inner">
          <div className="hero__svg">
            <img src={heroSvg} alt="hero-svg-divider" />
          </div>

          <div className="container">
            <div className="row">
              <div className="col-md-6 col-lg-7 col-xl-6 col-xxl-5">
                <div className="hero__title">
                  <div className="hero__logo">
                    <img src={heroLogo} alt="hero-svg-logo" />
                  </div>
                  <div className="hero__title">{ReactHtmlParser(title)}</div>
                </div>

                <div className="hero__content">{ReactHtmlParser(content)}</div>

                {buttons && (
                  <div className="btn-group">
                    {buttons.map((button, key) => {
                      return <Button key={key} {...button} />;
                    })}
                  </div>
                )}
              </div>

              {/* The LicensePlate Widget */}
              {widget && (
                <div className="col-md-6 col-lg-5 col-xl-4 offset-xl-1 offset-xxl-2">
                  <LicensePlateWidget widget={widget} />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

Hero.propTypes = {
  content: PropTypes.string,
  mode: PropTypes.oneOf(["dark", "light"]),
  title: PropTypes.string,
};

export default Hero;
