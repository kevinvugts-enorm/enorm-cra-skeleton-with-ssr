// Dependencies
import React from "react";
import ReactHtmlParser from "react-html-parser";
import PropTypes from "prop-types";

// Image Placeholder
import placeholder from "../../assets/images/hero-placeholder.jpg";

// Components
import LicensePlateWidget from "../../components/Widget";
import Button from "../../components/Button";

const HomepageHero = ({
  background_image,
  mode,
  title,
  content,
  widget,
  buttons,
}) => {
  return (
    <div className={`hero hero--mode-${mode} hero--height hero--homepage`}>
      <div className="hero__block">
        <div className="hero__inner">
          <div className="container">
            <div className="row">
              <div className="col-md-6 col-lg-5 col-xxl-4">
                <div className="hero__title">{ReactHtmlParser(title)}</div>
                <div className="hero__content">
                  {ReactHtmlParser(content)}

                  {buttons && (
                    <div className="btn-group">
                      {buttons.map((button, key) => {
                        return <Button key={key} {...button} />;
                      })}
                    </div>
                  )}
                </div>

                {/* The LicensePlate Widget */}
                {widget && <LicensePlateWidget widget={widget} />}
              </div>
            </div>
          </div>
          <div className="hero__image">
            {/* TODO: implement imagekit.io */}
            <img
              className="img-fluid"
              src={background_image[0].url || placeholder}
              alt={background_image[0].name || "Hero Placeholder"}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

HomepageHero.propTypes = {
  mode: PropTypes.oneOf(["dark", "light"]),
  title: PropTypes.string,
};

export default HomepageHero;
