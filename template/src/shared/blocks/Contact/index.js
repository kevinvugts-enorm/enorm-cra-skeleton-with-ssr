// Dependencies
import React from "react";
import { useHistory } from "react-router-dom";
import ReactHtmlParser from "react-html-parser";
import { useMutation } from "react-query";
import { Formik, Form as FormikForm } from "formik";

// Components
import ServerErrorMessage from "../../components/Error";
import Fields from "../../components/Form/Fields";

// seed
import {
  contactFormValidationSchema,
  contactFormFields,
  contactFormInitialValues,
} from "../../seed/forms";

// Utils
import { client } from "../../utils/api-client";

const Contact = (props) => {
  const history = useHistory();
  const { mutate, isLoading, isError, error } = useMutation((values) =>
    client("form-submissions", {
      data: values,
    }).then((res) => history.push("/home/bedankt"))
  );

  return (
    <section className="section section--mode-light">
      <div className="contact">
        <div className="container">
          <div className="row justify-content-between">
            <div className="col-lg-6">
              <div className="contact__text">
                {ReactHtmlParser(props.content)}
              </div>

              <div className="contact__text">
                {props.contact_content &&
                  ReactHtmlParser(props.contact_content)}
                <ul className="contact__list">
                  {props.contact_phone && (
                    <li>
                      <a
                        href={`tel:${props.contact_phone}`}
                        target="_blank"
                        rel="noreferrer noopener"
                        title={props.contact_phone}
                      >
                        <i className="fas fa-phone fa-fw"></i>
                        {props.contact_phone}
                      </a>
                    </li>
                  )}
                  {props.contact_email && (
                    <li>
                      <a
                        href={`mailto:${props.contact_email}`}
                        target="_blank"
                        rel="noreferrer noopener"
                        title={props.contact_email}
                      >
                        <i className="fas fa-envelope fa-fw"></i>
                        {props.contact_email}
                      </a>
                    </li>
                  )}
                </ul>
              </div>
            </div>

            <div className="col-lg-6 col-xxl-5">
              <div className="contact__form">
                <h3>
                  <strong>
                    Meld u aan als een van onze montagepartners.{" "}
                    <span>Wij nemen zo spoedig mogelijk contact met u op.</span>
                  </strong>
                </h3>
                <p className="contact__label">
                  * = verplichte in te vullen velden
                </p>

                {/* Server Error */}
                {isError && (
                  <ServerErrorMessage
                    error={error ? error.message : "Er is iets fout gegaan"}
                  />
                )}

                <Formik
                  initialValues={contactFormInitialValues}
                  validationSchema={contactFormValidationSchema}
                  onSubmit={(values) => mutate(values)}
                >
                  {(formik) => (
                    <FormikForm noValidate>
                      {/* Abstraction which renders an array of Field components based on the JSON */}
                      <Fields sections={contactFormFields} {...formik} />

                      <div className="contact__button">
                        <button
                          disabled={isLoading || !formik.isValid}
                          type="submit"
                          className="btn btn-theme-primary"
                        >
                          {isLoading && (
                            <i className="fas fa-cog fa-spin mr-1"></i>
                          )}
                          Aanvraag versturen
                        </button>
                      </div>
                    </FormikForm>
                  )}
                </Formik>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Contact;
