// Dependencies
import React from "react";
import { Link } from "react-router-dom";

const Breadcrumb = ({ crumbs, mode, currentPage }) => {
  return (
    crumbs.length > 1 && (
      <div className={`breadcrumb__container breadcrumb--mode-${mode}`}>
        <div className="container">
          <div className="row">
            <div className="col">
              <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                  {/*  Don't render a single breadcrumb. */}
                  {crumbs.map(({ name, path }, key) => (
                    <li
                      key={key}
                      aria-current={path === currentPage ? "page" : null}
                      className={`breadcrumb-item ${
                        path === currentPage ? "active" : ""
                      }`}
                    >
                      <Link className="breadcrumb-link" to={path} title={name}>
                        {name}
                      </Link>
                    </li>
                  ))}
                  {/* .breadcrumb-item */}
                </ol>
                {/* .breadcrumb */}
              </nav>
              {/* nav */}
            </div>
            {/* .col */}
          </div>
          {/* .row */}
        </div>
        {/* .container */}
      </div>
    )
  );
};

export default Breadcrumb;
