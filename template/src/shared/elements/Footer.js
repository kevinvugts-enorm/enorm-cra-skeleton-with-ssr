// Dependencies
import React from "react";
import { NavLink } from "react-router-dom";
// Images
import logo from "../assets/images/logo-primary-white.svg";
import placeholder from "../assets/images/keurmerk-placeholder.png";

const Footer = ({ footer }) => {
  const { keurmerk_logos, contact_email, phone_number } = footer;

  return (
    <footer className="footer">
      <div className="container">
        <div className="row justify-content-between">
          <div className="col-md-auto order-5 order-xl-1 mt-5 mt-xl-0">
            <div className="footer__column">
              <img className="img-fluid" src={logo} alt="Trekhaakmontage.nl" />
              <ul className="footer__contact">
                <li className="contact__item">Trekhaakmontage.nl</li>
                <li className="contact__item">
                  <a
                    className="contact__link"
                    href={`tel:${phone_number}`}
                    target="_blank"
                    rel="noreferrer noopener"
                    title="012 - 34 56 789"
                  >
                    <i className="fas fa-phone fa-fw"></i>
                    {phone_number}
                  </a>
                </li>
                <li className="contact__item">
                  <a
                    className="contact__link"
                    href={`mailto:${contact_email}`}
                    target="_blank"
                    rel="noreferrer noopener"
                    title="info@trekhaakmontage.nl"
                  >
                    <i className="fas fa-envelope fa-fw"></i>
                    {contact_email}
                  </a>
                </li>
              </ul>
              {/* .footer__list */}
            </div>
            {/* .footer__column */}
          </div>
          {/* .col */}

          <div className="col-md-auto order-1 order-xl-2">
            <div className="footer__column">
              <h6 className="d-none d-md-block">Producten</h6>
              <ul className="footer__list">
                <li className="list__item">
                  <NavLink
                    to="/home/trekhaken"
                    className="list__link"
                    title="Trekhaken"
                  >
                    Trekhaken
                  </NavLink>
                </li>
                <li className="list__item">
                  <NavLink
                    to="/home/kabelsets"
                    className="list__link"
                    title="Kabelsets"
                  >
                    Kabelsets
                  </NavLink>
                </li>
                <li className="list__item">
                  <NavLink
                    to="/home/fietsendragers"
                    className="list__link"
                    title="Fietsendragers"
                  >
                    Fietsendragers
                  </NavLink>
                </li>
              </ul>
              {/* .footer__list */}
            </div>
            {/* .footer__column */}
          </div>
          {/* .col */}

          <div className="col-md-auto order-2 order-xl-3">
            <div className="footer__column">
              <h6 className="d-none d-md-block">Informatie</h6>
              <ul className="footer__list">
                <li className="list__item">
                  <NavLink
                    to="/home/veelgestelde-vragen"
                    className="list__link"
                    title="Veelgestelde Vragen"
                  >
                    Veelgestelde vragen
                  </NavLink>
                </li>
                <li className="list__item">
                  <NavLink
                    to="/home/montage-locaties"
                    className="list__link"
                    title="Montagelocaties"
                  >
                    Montagelocaties
                  </NavLink>
                </li>
                <li className="list__item">
                  <NavLink
                    to="/home/over-trekhaakmontage"
                    className="list__link"
                    title="Over Trekhaakmontage.nl"
                  >
                    Over Trekhaakmontage.nl
                  </NavLink>
                </li>
                <li className="list__item">
                  <NavLink
                    to="/home/trekhaken"
                    className="list__link"
                    title="Wat kan er op uw auto?"
                  >
                    Wat kan er op uw auto?
                  </NavLink>
                </li>
              </ul>
              {/* .footer__list */}
            </div>
            {/* .footer__column */}
          </div>
          {/* .col */}

          <div className="col-md-auto order-3 order-xl-4">
            <div className="footer__column">
              <h6 className="d-none d-md-block">Zakelijk</h6>
              <ul className="footer__list">
                <li className="list__item d-md-none">
                  <NavLink
                    to="/home/zakelijk-partner"
                    className="list__link"
                    title="Zakeljk"
                  >
                    Zakelijk
                  </NavLink>
                </li>
                <li className="list__item d-none d-md-block">
                  <NavLink
                    to="/home/over-trekhaakmontage"
                    className="list__link"
                    title="Alle informatie"
                  >
                    Alle informatie
                  </NavLink>
                </li>
                <li className="list__item d-none d-md-block">
                  <NavLink
                    to="/home/partner"
                    className="list__link"
                    title="Aanmelden als partner"
                  >
                    Aanmelden als partner
                  </NavLink>
                </li>
                <li className="list__item d-none d-md-block">
                  <a
                    className="list__link"
                    href="#"
                    target="_blank"
                    referrer="noreferrer noopener"
                    title="Login"
                  >
                    Login
                  </a>
                </li>
              </ul>
              {/* .footer__list */}
            </div>
            {/* .footer__column */}
          </div>
          {/* .col */}

          <div className="col-md-auto order-4 order-xl-5 mt-5 mt-lg-0">
            <div className="footer__column">
              <h6>Keurmerken</h6>
              <div className="footer__images">
                {keurmerk_logos.map((logo, key) => {
                  return (
                    <div key={key} className="footer__image">
                      <img
                        className="img-fluid"
                        src={logo.url || placeholder}
                        alt={logo.name}
                      />
                    </div>
                  );
                })}
                {/* .footer__image */}
              </div>
              {/* .footer__images */}
            </div>
            {/* .footer__column */}
          </div>
          {/* .col */}
        </div>
        {/* .row */}

        <div className="row justify-content-center">
          <div className="col-auto">
            <ul className="footer__links">
              <li className="links__item">
                <NavLink
                  to="/home/disclaimer"
                  className="links__link"
                  title="Disclaimer"
                >
                  Disclaimer
                </NavLink>
              </li>
              <li className="links__item">
                <NavLink
                  to="/home/privacy"
                  className="links__link"
                  title="Privacy"
                >
                  Privacy
                </NavLink>
              </li>
              <li className="links__item">
                <NavLink
                  to="/home/cookies"
                  className="links__link"
                  title="Cookies"
                >
                  Cookies
                </NavLink>
              </li>
              <li className="links__item">
                <NavLink
                  to="/home/algemene-voorwaarden"
                  className="links__link"
                  title="Algemene voorwaarden"
                >
                  Algemene voorwaarden
                </NavLink>
              </li>
            </ul>
            {/* .footer__links */}
          </div>
          {/* .col */}
        </div>
        {/* .row */}
      </div>
      {/* .container */}
    </footer>
  );
};

export default Footer;
