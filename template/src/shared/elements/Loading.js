// Dependencies
import React from "react";
// Assets
import logo from "../assets/images/logo-primary-white.svg";

const Loading = () => {
  return (
    <div className="loader-wrapper">
      <i className="loader__icon fas fa-cog fa-spin"></i>
      <img className="loader__logo" src={logo} alt="Trekhaak Montage" />
    </div>
  );
};

export default Loading;
