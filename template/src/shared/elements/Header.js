// Dependencies
import React, { useReducer } from "react";
import { NavLink } from "react-router-dom";
// Assets
import logo from "../assets/images/logo-primary-white.svg";

// Utils
import { useBreakpoint } from "../hooks";

const initialState = {
  navigation: false,
  dropdown: false,
};

function reducer(state, action) {
  switch (action.type) {
    case "dropdown":
      return { ...state, dropdown: !state.dropdown };
    case "navigation":
      return {
        ...state,
        navigation: !state.navigation,
        dropdown: state.navigation === false && false,
      };
    default:
      throw new Error(`Unhandled action type ${action.type}`);
  }
}

const Header = (props) => {
  const breakpoint = useBreakpoint();
  const [{ navigation, dropdown }, dispatch] = useReducer(
    reducer,
    initialState
  );

  return (
    <header>
      <nav className="navbar navbar-expand-lg">
        <div className="container">
          <NavLink className="navbar-brand" to="/home" title="Trekhaak Montage">
            <img src={logo} alt="Trekhaak Montage" />
          </NavLink>

          <div
            className={
              navigation ? "navbar-collapse is-active" : "navbar-collapse"
            }
          >
            {/* Navbar */}
            <ul className="navbar-nav">
              <li
                className={
                  dropdown ? "nav-item dropdown show" : "nav-item dropdown"
                }
                onClick={() => {
                  breakpoint.size <= 992 && dispatch({ type: "dropdown" });
                }}
              >
                <a
                  className="nav-link dropdown-toggle"
                  id="navbarDropdownMenuLink"
                  role="button"
                  data-toggle={dropdown}
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Producten
                </a>
                <div
                  className={dropdown ? "dropdown-menu show" : "dropdown-menu"}
                  aria-labelledby="navbarDropdownMenuLink"
                >
                  <NavLink to="/home/trekhaken" className="dropdown-item">
                    Trekhaken
                  </NavLink>
                  <NavLink to="/home/kabelsets" className="dropdown-item">
                    Kabelsets
                  </NavLink>
                  <NavLink to="/home/fietsendragers" className="dropdown-item">
                    Fietsendragers
                  </NavLink>
                </div>
              </li>
              <li className="nav-item">
                <NavLink to="/home/veelgestelde-vragen" className="nav-link">
                  Veelgestelde vragen
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/home/montage-locaties" className="nav-link">
                  Montagelocaties
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/home/over-trekhaakmontage" className="nav-link">
                  Over ons <span className="sr-only">(current)</span>
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/home/zakelijk-partner" className="nav-link">
                  Zakelijke partner
                </NavLink>
              </li>
              <li className="nav-btn__partner nav-item">
                <NavLink
                  to="/partner-login"
                  className="btn btn-block btn-outline-white"
                  role="button"
                >
                  Partner Login
                </NavLink>
              </li>
            </ul>
          </div>

          {/* Navbar Actions */}
          <div className="navbar__actions">
            <div className="navbar__phone">
              <a href="tel:+31123456789" role="button" title="012 - 34 56 789">
                <i className="fas fa-phone fa-rotate-90"></i>
                <span>012 - 34 56 789</span>
              </a>
            </div>
            {/* .navbar__phone */}
            <div className="navbar__price">
              <NavLink
                to="/configurator"
                className="btn btn-yellow btn-sm"
                role="button"
              >
                Bereken uw prijs
              </NavLink>
            </div>
            {/* .navbar__price */}
            <div className="navbar__partner">
              <NavLink
                to="/configurator"
                href="/"
                role="button"
                rel="noopener noreferrer"
                target="_blank"
                title="Partner login"
              >
                <span>Partner login</span>
                <i className="fad fa-user-lock"></i>
              </NavLink>
            </div>
            {/* .navbar__partner */}
          </div>

          {/* Hamburger */}
          <button
            className={
              navigation
                ? "hamburger hamburger--spin is-active"
                : "hamburger hamburger--spin"
            }
            type="button"
            aria-label="Toggle navigation"
            aria-controls="offcanvas"
            aria-expanded="false"
            onClick={() => dispatch({ type: "navigation" })}
          >
            <span className="hamburger-box">
              <span className="hamburger-inner"></span>
            </span>
          </button>
        </div>
      </nav>
    </header>
  );
};

export default Header;
