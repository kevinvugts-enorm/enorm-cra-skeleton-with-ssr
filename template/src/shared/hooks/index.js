import React, { useState, useEffect } from "react";

//a Util function that will conver the absolute width into breakpoints
function getBreakPoint(windowWidth) {
  if (windowWidth) {
    if (windowWidth <= 576) {
      return { name: "xs", size: windowWidth };
    } else if (windowWidth <= 768) {
      return { name: "sm", size: windowWidth };
    } else if (windowWidth <= 992) {
      return { name: "md", size: windowWidth };
    } else if (windowWidth <= 1200) {
      return { name: "lg", size: windowWidth };
    } else if (windowWidth <= 1500) {
      return { name: "xl", size: windowWidth };
    } else {
      return { name: "xxl", size: windowWidth };
    }
  } else {
    return undefined;
  }
}

function useBreakpoint() {
  const isWindowClient = typeof window === "object";

  const [windowSize, setWindowSize] = useState(
    isWindowClient ? getBreakPoint(window.innerWidth) : undefined
  );

  useEffect(() => {
    //a handler which will be called on change of the screen resize
    function setSize() {
      setWindowSize(getBreakPoint(window.innerWidth));
    }

    if (isWindowClient) {
      //register the window resize listener
      window.addEventListener("resize", setSize);

      //unregister the listerner
      return () => window.removeEventListener("resize", setSize);
    }
  }, [isWindowClient, setWindowSize]);

  return windowSize;
}

function useSwiperSlides(slidesPerViewOverride) {
  const breakpoint = useBreakpoint();

  const defaultSlidesPerView = {
    xs: 1,
    sm: 1,
    md: 2,
    lg: 5,
    xl: 5,
    xxl: 5,
  };

  const slidesPerView = slidesPerViewOverride || defaultSlidesPerView;

  return breakpoint ? slidesPerView[breakpoint.name] : 5;
}

/* ==========================================================================
Reducers
========================================================================== */
function asyncReducer(state, action) {
  switch (action.type) {
    case "pending": {
      return { status: "pending", data: null, error: null };
    }
    case "resolved": {
      return { status: "resolved", data: action.data, error: null };
    }
    case "rejected": {
      return { status: "rejected", data: null, error: action.error };
    }
    case "reset": {
      return { status: "rejected", data: null, error: null };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

/* ==========================================================================
Used in scenarios when we want to prevent state updates when unmounting can happen
========================================================================== */
function useSafeDispatch(dispatch) {
  const mountedRef = React.useRef(false);

  // side effect only called on mount / unmount to internally keep track of mount state
  React.useLayoutEffect(() => {
    mountedRef.current = true;

    return () => (mountedRef.current = false);
  }, []);

  // make sure our own function of dispatch is stable over re-renders
  return React.useCallback(
    (...args) => {
      if (mountedRef.current) {
        dispatch(...args);
      }
    },
    [dispatch]
  );
}

/* ==========================================================================
Hook which helps us to make async requests with status resolving. With this
we prevent isloading booleans, error states etc.
========================================================================== */
const useAsync = (initialState) => {
  const [state, unsafeDispatch] = React.useReducer(asyncReducer, {
    status: "idle",
    data: null, // previously markers
    error: null,
    ...initialState,
  });

  // memoized dispatch it returns
  const dispatch = useSafeDispatch(unsafeDispatch);

  const getStatusColor = () => {
    switch (state.status) {
      case "idle":
        return "aqua";
      case "resolved":
        return "lime";
      case "error":
      case "rejected":
        return "red";
      default:
        return "orange";
    }
  };

  // we don't have to provide any dependencies since dispatch is stable coming from useReducer  and will never change
  // The same for the promise, but since we accepting this as an argument, so it wo't be necesary to keep track of either.

  // runs an async operation and updates the internal status state
  // Because it makes use of useCallback the run function will be stable over time
  const run = React.useCallback(
    (promise) => {
      dispatch({ type: "pending" });

      promise
        .then((data) => data && dispatch({ type: "resolved", data }))
        .catch((error) => {
          dispatch({ type: "rejected", error });
        });
    },
    [dispatch]
  );

  console.log(`%c status: ${state.status}`, `color: ${getStatusColor()};`);

  return {
    // using the same names that react-query uses for convenience
    isIdle: state.status === "idle",
    isLoading: state.status === "pending",
    isError: state.status === "rejected",
    isSuccess: state.status === "resolved",
    status: state.status,
    run,
    ...state,
  };
};

export { useBreakpoint, useSwiperSlides, useAsync };
