import * as Yup from "yup";

import { createYupSchema } from "../../utils/yup";

const contactFormFields = [
  {
    sectionName: "Bedrijfsinformatie:",
    fields: [
      {
        layout: "full",
        label: "Naam van uw autobedrijf:",
        name: "company_name",
        type: "text",
        validationType: "string",
        validation: [
          {
            type: "required",
            params: ["De bedrijfsnaam is een verplicht veld."],
          },
        ],
      },
      {
        layout: "full",
        label: "Website van uw autobedrijf:",
        name: "company_website",
        type: "text",
        validationType: "string",
        validation: [
          {
            type: "required",
            params: ["De bedrijfswebsite is een verplicht veld."],
          },
        ],
      },

      {
        layout: "full",
        label: "Straat en huisnummer",
        name: "address",
        type: "text",
        validationType: "string",
        validation: [
          {
            type: "required",
            params: ["Straat en huisnummer is een verplicht veld."],
          },
        ],
      },

      {
        layout: "half",
        addressAutoFill: true,
        fields: [
          {
            label: "Postcode:",
            name: "postal_code",
            type: "text",
            validationType: "string",
            validation: [
              {
                type: "required",
                params: ["Postcode is een verplicht veld."],
              },
            ],
          },
          {
            label: "Stad:",
            name: "city",
            type: "text",
            validationType: "string",
            validation: [
              {
                type: "required",
                params: ["Stad is een verplicht veld."],
              },
            ],
          },
        ],
      },
      {
        label: "Kvk-nummer:",
        name: "kvk_number",
        type: "text",
        validationType: "number",
        validation: [
          {
            type: "typeError",
            params: ["Het kvk nummer mag alleen cijfers bevatten"],
          },
          {
            type: "required",
            params: ["Het kvk nummer is een verplicht veld."],
          },
          {
            type: "test",
            params: [
              "len",
              "Het kvk nummer moet bestaan uit 8 karakters",
              (val) => val && val.toString().length === 8,
            ],
          },
        ],
      },
    ],
  },
  {
    sectionName: "Contactgegevens:",
    fields: [
      {
        label: "E-mailadres:",
        name: "email",
        type: "email",
        validationType: "string",
        validation: [
          {
            type: "required",
            params: ["Email is een verplicht veld."],
          },
          {
            type: "email",
            params: ["Ongeldig email adres"],
          },
        ],
      },
      {
        label: "Telefoonnummer:",
        name: "phone_number",
        type: "text",
        validationType: "string",
        validation: [
          {
            type: "required",
            params: ["Het telefoonnummer is een verplicht veld."],
          },
        ],
      },
    ],
  },
  {
    sectionName: "Eventuele opmerkingen",
    fields: [
      {
        label: "Vraag of opmerking:",
        name: "comments",
        type: "textarea",
      },
    ],
  },
];

const contactFormInitialValues = {
  company_name: "",
  company_website: "",
  email: "",
  phone_number: "",
  address: "",
  postal_code: "",
  city: "",
  comments: "",
  kvk_number: "",
};

// Validation schema
const contactSchema = contactFormFields
  .reduce((prev, curr) => {
    if (curr.fields.find((obj) => obj.layout === "half")) {
      return [
        ...prev,
        ...curr.fields.find((obj) => obj.layout === "half").fields,
        ...curr.fields,
      ];
    } else {
      return [...prev, ...curr.fields];
    }
  }, [])
  .reduce(createYupSchema, {});

const contactFormValidationSchema = Yup.object().shape(contactSchema);

export {
  contactFormFields,
  contactFormInitialValues,
  contactFormValidationSchema,
};

//street_name: Yup.string().required("De straatnaam is een verplicht veld."),
//province: Yup.string().required("De provincie is een verplicht veld."),
//city: Yup.string().required("De plaatsnaam is een verplicht veld."),
