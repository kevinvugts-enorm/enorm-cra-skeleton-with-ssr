// Dependencies
import React, { Fragment, useState, useEffect } from "react";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from "react-places-autocomplete";

const AutoComplete = ({ setLocation, resetMarkers }) => {
  const [error, setError] = useState("");
  const [address, setAddress] = useState("");
  const [dropdownActive, setDropdownActive] = useState(false);

  const handleError = (status, clearSuggestions) => {
    // Set error state with status
    setError(status);
    // Clear dropdown when Google Maps API returns an error
    clearSuggestions();
  };

  const handleChange = (address) => {
    // Empty error state
    setError("");
    // Set address state
    setAddress(address);
    // Set dropdown state true
    setDropdownActive(true);

    if (!address) {
      resetMarkers();
    }
  };

  const handleSelect = (address) => {
    geocodeByAddress(address)
      .then((results) => getLatLng(results[0]))
      .then((latLng) => {
        // Set address state
        setAddress(address);
        // Set markers
        setLocation(latLng);
      })
      .catch((error) => setError(error));

    // Set dropdown state false
    setDropdownActive(false);
  };

  // Limit autocomplete predictions toward the Netherlands
  const searchOptions = {
    componentRestrictions: {
      country: ["nl"],
    },
  };

  useEffect(() => {
    // Set dropdown state false if address state is empty
    if (!address) {
      setDropdownActive(false);
    }
  }, [address]);

  return (
    <form className="dealer__form form-inline">
      <label className="sr-only" htmlFor="autocomplete">
        Postcode
      </label>
      <PlacesAutocomplete
        value={address}
        onChange={handleChange}
        onSelect={handleSelect}
        onError={handleError}
        searchOptions={searchOptions}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
          <Fragment>
            <input
              {...getInputProps({
                placeholder: "Zoek via uw postcode",
                className: "form-control",
              })}
            />
            <i className="dealer__form-icon far fa-search"></i>
            <div
              className={`dealer__autocomplete-loader ${
                dropdownActive ? "active" : ""
              }`}
            >
              <i className="fas fa-cog fa-spin"></i>
            </div>
            <div
              className={`dealer__autocomplete ${
                dropdownActive ? "active" : ""
              }`}
            >
              {/* If error show status message */}
              {error && (
                <div className="dealer__autocomplete-item disabled">
                  {error}
                </div>
              )}
              {/* Show Autocomplete suggestions */}
              {suggestions.map((suggestion) => {
                const className = suggestion.active
                  ? "dealer__autocomplete-item active"
                  : "dealer__autocomplete-item";
                return (
                  <div
                    {...getSuggestionItemProps(suggestion, {
                      className,
                    })}
                    key={suggestion.placeId}
                  >
                    <p>{suggestion.description}</p>
                  </div>
                );
              })}
            </div>
          </Fragment>
        )}
      </PlacesAutocomplete>
    </form>
  );
};

export default AutoComplete;
