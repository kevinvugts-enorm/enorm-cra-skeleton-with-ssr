// Dependencies
import React from "react";
import { useGoogleMapsActiveMarker } from "../../../context/maps";

const LocationList = ({ status, dealers }) => {
  const { setActiveMarkerId } = useGoogleMapsActiveMarker();

  const renderDealers = (dealers) => {
    return dealers.map((item) => {
      return (
        <div
          key={item.uuid}
          className="dealer__item media"
          onClick={() => setActiveMarkerId(item.uuid)}
        >
          <div className="media-body">
            <h5 className="dealer__city">{item.city}</h5>
            <small className="dealer__name">{item.title}</small>
            <button className="dealer__info" type="button">
              <i className="far fa-info-circle"></i>Meer informatie
            </button>
          </div>
          {item.distance ? (
            <p className="dealer__distance">
              <i className="fas fa-map-marker-alt"></i>
              {`${item.distance} km`}
            </p>
          ) : null}
        </div>
      );
    });
  };

  return (
    <div className="dealer__list">
      {status === "pending" && (
        <div className="dealer__item">
          <i className="fas fa-cog fa-spin"></i>
        </div>
      )}
      {dealers ? (
        renderDealers(dealers)
      ) : (
        <h5>Geen montagelocaties gevonden</h5>
      )}
    </div>
  );
};

export default LocationList;
