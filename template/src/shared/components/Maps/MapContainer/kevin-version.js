// Dependencies
import React, {
  useState,
  createRef,
  useCallback,
  useEffect,
  useRef,
} from "react";
import { Map, Marker, GoogleApiWrapper, InfoWindow } from "google-maps-react";
// Images
import markerIcon from "../../../assets/images/map-marker.svg";

import { useGoogleMapsActiveMarker } from "../../../context/maps";

const MapContainer = ({ markers, ...props }) => {
  const [showInfoWindow, setShowInfoWindow] = useState(false);
  const [infoWindowContent, setShowInfoWindowContent] = useState("");
  const { activeMarkerId } = useGoogleMapsActiveMarker();

  const [activeMarker, setActiveMarker] = useState(null);

  const mapRef = createRef();
  const markerRefs = useRef([]);

  // Center map based on marker lat lng
  const fitBounds = useCallback(
    (markers) => {
      const bounds = new props.google.maps.LatLngBounds();
      markers.forEach((item) => {
        const { lat, lng } = item;
        bounds.extend({ lat, lng });
      });
      // Set max zoom (useful when only one marker is shown)
      mapRef.current.map.setOptions({ maxZoom: 14 });
      mapRef.current.map.fitBounds(bounds);
    },
    [mapRef, props.google.maps.LatLngBounds]
  );

  const renderInfoWindow = () => {
    return (
      <InfoWindow
        marker={activeMarker}
        onClose={() => {
          setShowInfoWindow(false);
          setActiveMarker(null);
        }}
        visible={showInfoWindow}
      >
        <div className="card">
          <div className="card-body">
            <h5 className="card-title text-theme-primary">
              {infoWindowContent.title}
            </h5>
            <table className="table table-borderless">
              <tbody>
                <tr>
                  <th className="py-0 pr-1 pl-0" scope="row">
                    <i className="fas fa-map-marker-alt mr-1"></i>
                  </th>
                  <td className="p-0">Stationsplein 3, 4811 BB Breda</td>
                </tr>
                <tr>
                  <th className="py-0 pr-1 pl-0" scope="row">
                    <i className="fas fa-envelope mr-1"></i>
                  </th>
                  <td className="p-0">
                    <a href="mailto:hello@enorm.com" target="_self">
                      hello@enorm.com
                    </a>
                  </td>
                </tr>
                <tr>
                  <th className="py-0 pr-1 pl-0" scope="row">
                    <i className="fas fa-phone-alt mr-1"></i>
                  </th>
                  <td className="p-0">
                    <a href="tel:0310612345678" target="_self">
                      +31 (6) 12345678
                    </a>
                  </td>
                </tr>
              </tbody>
            </table>

            <p className="text-theme-primary">
              <strong>Openingstijden:</strong>
            </p>
            <table className="table table-borderless">
              <tbody>
                <tr>
                  <th className="py-0 pr-1 pl-0" scope="row">
                    Maandag t/m vrijdag
                  </th>
                  <td className="p-0">08:30 - 17:00</td>
                </tr>
                <tr>
                  <th className="py-0 pr-1 pl-0" scope="row">
                    Zaterdag
                  </th>
                  <td className="p-0">08:30 - 13:00</td>
                </tr>
                <tr>
                  <th className="py-0 pr-1 pl-0" scope="row">
                    Zondag
                  </th>
                  <td className="p-0">Gesloten</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </InfoWindow>
    );
  };

  const handleMarkerClick = (props, marker) => {
    setActiveMarker(marker);
  };

  React.useEffect(() => {
    if (!activeMarker) {
      return;
    }
    setShowInfoWindow(true);
    setShowInfoWindowContent(activeMarker.content);
  }, [activeMarker]);

  useEffect(() => {
    markers && fitBounds(markers);
  }, [markers, fitBounds]);

  // If activeMarkerId get marker ref and open info window
  useEffect(() => {
    const currentMarker = activeMarkerId && markerRefs.current[activeMarkerId];

    if (currentMarker && currentMarker.props.content.uuid === activeMarkerId) {
      console.log("HANDLING CLICK OF MARKER VIA BUTTON");
      handleMarkerClick(currentMarker.props, currentMarker.marker);
    }
  }, [activeMarkerId]);

  return (
    <Map
      ref={mapRef}
      google={props.google}
      zoom={8}
      disableDefaultUI={true}
      style={{ borderRadius: 20, overflowAnchor: "none" }}
      initialCenter={{
        lat: 52.090736,
        lng: 5.12142,
      }}
    >
      {markers &&
        markers.map((item) => {
          return (
            <Marker
              key={item.uuid}
              name={item.title}
              position={{ lat: item.lat, lng: item.lng }}
              icon={{
                url: markerIcon,
                anchor: new props.google.maps.Point(16, 16),
                scaledSize: new props.google.maps.Size(32, 32),
              }}
              content={item}
              onClick={handleMarkerClick}
              ref={(marker) => (markerRefs.current[item.uuid] = marker)}
            />
          );
        })}

      {activeMarker && renderInfoWindow()}
    </Map>
  );
};

export default GoogleApiWrapper({
  apiKey: APP_CONFIG.googleMapsApiKey,
})(MapContainer);
