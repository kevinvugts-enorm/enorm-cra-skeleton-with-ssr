// Dependencies
import React from "react";
import { Link } from "react-router-dom";

function underscoreReplace(string) {
  let newString = string.replaceAll("_", "-");

  return newString;
}

const Button = ({ customClassnames, buttonType, ...props }) => {
  const classNames =
    props.type_button === "outlined"
      ? `btn btn-outline-${underscoreReplace(props.theme)} ${customClassnames}`
      : `btn btn-${underscoreReplace(props.theme)} ${customClassnames}`;

  const url = props.file
    ? props.file.url
    : props.page_url
    ? props.page_url.page_path
    : null;

  switch (buttonType) {
    case "submit":
    case "button":
      return (
        <button id={props.anchor_tag} className={classNames} type={buttonType}>
          {props.title}
        </button>
      );
    default:
      return props.file ? (
        <a
          id={props.anchor_tag}
          href={url ? url : ""}
          target={props.target_button}
          className={classNames}
          role="button"
        >
          {props.title}
        </a>
      ) : (
        <Link
          id={props.anchor_tag}
          to={url ? url : ""}
          target={props.target_button}
          className={classNames}
          role="button"
        >
          {props.title}
        </Link>
      );
  }
};

export default Button;
