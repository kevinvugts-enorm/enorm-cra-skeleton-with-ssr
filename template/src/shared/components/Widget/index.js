import React from "react";
import ReactHtmlParser from "react-html-parser";

// Assets
import licensePlatePrefix from "../../assets/images/license_plate_nl_prefix.svg";

// Services
import { licensePlateFormatter } from "../../services";

// Formats a string to a licenseplate syntax xxxxxxx -> xx-xx-xx
const { formatLicensePlate } = licensePlateFormatter;

import Button from "../Button";

const LicensePlateWidget = ({ widget }) => {
  const [plate, setPlate] = React.useState("");

  function handleSubmit(e) {
    e.preventDefault();
  }

  return (
    <div className="hero__form">
      <form onSubmit={handleSubmit}>
        {/* Form Title */}
        <div className="hero__form-title">{ReactHtmlParser(widget.title)}</div>

        {/* Form LicensePlate Input */}
        <div className="hero__form-input">
          <img src={licensePlatePrefix} />

          <div className="form-group">
            <input
              aria-label="Kenteken"
              id="license-plate"
              maxLength="8"
              name="license_plate"
              onChange={(e) => setPlate(formatLicensePlate(e.target.value))}
              type="text"
              value={plate}
            />
          </div>
        </div>

        {/* Form Actions */}
        <div className="hero__form-actions">
          <Button
            customClassnames="hero__form-button-1"
            buttonType="submit"
            {...widget.button_one}
          />

          <span>{widget.separatorText}</span>

          <Button
            customClassnames="hero__form-button-2"
            {...widget.button_two}
          />
        </div>
      </form>
    </div>
  );
};

export default LicensePlateWidget;
