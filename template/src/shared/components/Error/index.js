import React from "react";

// Bootstrap
import { Alert } from "react-bootstrap";

export default ({ error, multiple }) => {
  function renderMultiple() {
    return (
      <Alert className="text-center" variant="danger">
        {Object.keys(error).map((k, i) => (
          <p key={i} className="mb-0" style={{ fontSize: 12 }}>
            {error[k]}
          </p>
        ))}
      </Alert>
    );
  }

  function renderSingle() {
    return (
      <Alert className="text-center" variant="danger">
        <p className="mb-0">{error}</p>
      </Alert>
    );
  }

  return multiple && error ? renderMultiple() : renderSingle();
};
