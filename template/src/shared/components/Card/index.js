// Dependencies
import React from "react";
import { Link } from "react-router-dom";
import ReactHtmlParser from "react-html-parser";

// Component
import Button from "../Button";

// TODO refactor use for multiple strings (for all props)
function underscoreReplace(string) {
  let newString = string.replaceAll("_", "-");

  return newString;
}

function linkWrapper(children, page_url) {
  if (page_url) {
    return (
      <Link className="card__link" to={page_url}>
        {children}
      </Link>
    );
  } else {
    return <div className="card__link">{children}</div>;
  }
}

const ConditionalLinkWrapper = ({ condition, renderWrapper, children }) =>
  renderWrapper(children);

const Card = (props) => {
  const hasLink = props.page_url !== undefined && props.page_url !== null;

  return (
    <div
      className={`card card-template__${underscoreReplace(
        props.template
      )} card-column__${underscoreReplace(props.column_width)}`}
    >
      <ConditionalLinkWrapper
        condition={hasLink}
        renderWrapper={(children) =>
          linkWrapper(children, props.page_url ? props.page_url.page_path : "")
        }
      >
        {props.thumbnail && props.thumbnail.image.length > 0 && (
          <div className="card-img">
            <img
              src={props.thumbnail.image[0].url}
              alt={props.thumbnail.image[0].name}
            />
            {props.thumbnail.label && (
              <span
                className={`card__label card__label--${underscoreReplace(
                  props.thumbnail.label_theme
                )}`}
              >
                {props.thumbnail.label}
              </span>
            )}
          </div>
        )}
        {/* If no image add className pt-0 to card-body */}
        <div className="card-body">
          <div className="card__content">
            <h5 className="card-title">{props.title}</h5>
            {<div className="card-text">{ReactHtmlParser(props.content)}</div>}
          </div>
          <div className="card-footer">
            {props.buttons && (
              <div className="btn-group">
                {props.buttons.map((button, key) => {
                  return <Button key={key} buttonType="button" {...button} />;
                })}
              </div>
            )}
          </div>
        </div>
      </ConditionalLinkWrapper>
    </div>
  );
};

export default Card;
