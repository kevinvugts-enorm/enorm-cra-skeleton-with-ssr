import React from "react";
import { ErrorMessage, Field, useFormikContext } from "formik";

const Fields = (props) => {
  const { errors, touched } = useFormikContext();

  const renderLayout = (input, key) => {
    const {
      label,
      name,
      layout,
      type,
      // addressAutoFill,
      fields,
      helperText,
    } = input;

    if (layout === "half") {
      const inputOne = fields[0];
      const inputTwo = fields[1];

      return (
        <React.Fragment key={key}>
          <div className="form-group">
            <div className="row">
              <div className="col-sm-6 mb-3 mb-sm-0">
                <label htmlFor={inputOne.name}>
                  {inputOne.label}
                  {inputOne.validation && inputOne.validation[0].helperText && (
                    <small id={inputOne.name} className="form-text">
                      {inputOne.helperText}
                    </small>
                  )}
                  {inputOne.validation &&
                    inputOne.validation[0].type === "required" && (
                      <span className="contact__required">*</span>
                    )}
                </label>

                <Field
                  name={inputOne.name}
                  aria-describedby={inputOne.name}
                  className={`form-control ${
                    touched[name] && errors[name] ? "is-invalid" : ""
                  }`}
                />
                <ErrorMessage
                  name={inputOne.name}
                  className="errorMessage"
                  component="div"
                />
              </div>

              {/* .col */}
              <div className="col-sm-6">
                <label htmlFor={inputOne.name}>
                  {inputTwo.label}
                  {inputTwo.validation && inputTwo.validation[0].helperText && (
                    <small id={inputTwo.name} className="form-text">
                      {inputTwo.helperText}
                    </small>
                  )}
                  {inputTwo.validation &&
                    inputTwo.validation[0].type === "required" && (
                      <span className="contact__required">*</span>
                    )}
                </label>

                <Field
                  name={inputTwo.name}
                  aria-describedby={inputTwo.name}
                  className={`form-control ${
                    touched[name] && errors[name] ? "is-invalid" : ""
                  }`}
                />
                <ErrorMessage
                  name={inputTwo.name}
                  className="errorMessage"
                  component="div"
                />
              </div>
            </div>
          </div>
          {/* addressAutoFill && (
            <div className="form-group">
              <p>
                <span>Straatnaam 23</span>
                <span>1234AB Plaatsnaam</span>
                <span>Provincie Nederland</span>
                <a href="#" title=">Wijzig het adres">
                  Wijzig het adres
                </a>
              </p>
            </div>
          ) */}
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment key={key}>
          <div className="form-group">
            <label htmlFor={name}>
              {label}
              {helperText && (
                <small id={name} className="form-text">
                  {helperText}
                </small>
              )}
              {input.validation && input.validation[0].type === "required" && (
                <span className="contact__required">*</span>
              )}
            </label>

            <Field
              type={type}
              component={type === "textarea" ? "textarea" : "input"}
              name={name}
              className={`form-control ${
                touched[name] && errors[name] ? "is-invalid" : ""
              }`}
              aria-describedby={name}
              {...props.formik}
            />

            <ErrorMessage
              name={name}
              className="errorMessage"
              component="div"
            />
          </div>
        </React.Fragment>
      );
    }
  };

  const renderFields = (mappedFields) => {
    return mappedFields.map((input, key) => renderLayout(input, key));
  };

  return props.sections.map((section, key) => {
    return (
      <React.Fragment key={key}>
        <h4 className="contact__form-section-title">
          <strong>{section.sectionName}</strong>
        </h4>
        {renderFields(section.fields)}
      </React.Fragment>
    );
  });
};

export default Fields;
