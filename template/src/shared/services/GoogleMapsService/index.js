export default class GoogleMapsService {
  getDistanceFromLatLonInKm(lat1, lng1, lat2, lng2) {
    const R = 6371; // Radius of the earth in km
    const dLat = this.deg2rad(lat2 - lat1); // deg2rad below
    const dLng = this.deg2rad(lng2 - lng1);
    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) *
        Math.cos(this.deg2rad(lat2)) *
        Math.sin(dLng / 2) *
        Math.sin(dLng / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c; // Distance in km
    return d;
  }

  deg2rad(deg) {
    return deg * (Math.PI / 180);
  }

  setMarkers(data, latLng) {
    let closest = 0;

    const markers = [];

    data.forEach((item) => {
      // Only push markers with latitude & longitude
      if (item.fieldData.Latitude && item.fieldData.Longitude) {
        // If location filter markers by distance
        if (latLng) {
          const distance = this.getDistanceFromLatLonInKm(
            latLng.lat,
            latLng.lng,
            item.fieldData.Latitude,
            item.fieldData.Longitude
          );
          if (distance <= closest || closest === 0) {
            closest = distance;
            markers.push(this.formatMarker(item, closest));
          }
        } else {
          markers.push(this.formatMarker(item));
        }
      }
    });

    return new Promise((resolve, reject) => {
      markers
        ? resolve(markers)
        : reject(
            "Could not set markers cause we did not receive a result from the API"
          );
    });
  }

  formatMarker(item, distance) {
    return {
      uuid: item.fieldData.UUID_Vestiging,
      title: item.fieldData.NaamVestiging,
      city: item.fieldData.Woonplaats,
      lat: parseFloat(item.fieldData.Latitude),
      lng: parseFloat(item.fieldData.Longitude),
      distance: Math.ceil(distance),
    };
  }

  getMarkers(latLng, data) {
    return this.setMarkers(data, latLng);
  }
}
