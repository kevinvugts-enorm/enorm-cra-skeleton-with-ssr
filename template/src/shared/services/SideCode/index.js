 /* ==========================================================================
 Licenseplate Formatter:
 Used to format a string of xxxxxx to the format of a license plate xx-xx-xx
 
 Pattern: Module Pattern
 Explanation: Uses the module pattern and encapsulates everything which does not belong in
 the global scope. Thus it results in a reduction in the likelihood of our
 function names conflicting with other functions defined in the scope
 ========================================================================== */
const licensePlateFormatter = (function () {
  const sideCodes = [
    /^[a-zA-Z]{2}[\d]{2}[\d]{2}$/, // 1 XX-99-99
    /^[\d]{2}[\d]{2}[a-zA-Z]{2}$/, // 2 99-99-XX
    /^[\d]{2}[a-zA-Z]{2}[\d]{2}$/, // 3 99-XX-99
    /^[a-zA-Z]{2}[\d]{2}[a-zA-Z]{2}$/, // 4 XX-99-XX
    /^[a-zA-Z]{2}[a-zA-Z]{2}[\d]{2}$/, // 5 XX-XX-99
    /^[\d]{2}[a-zA-Z]{2}[a-zA-Z]{2}$/, // 6 99-XX-XX
    /^[\d]{2}[a-zA-Z]{3}[\d]{1}$/, // 7 99-XXX-9
    /^[\d]{1}[a-zA-Z]{3}[\d]{2}$/, // 8 9-XXX-99
    /^[a-zA-Z]{2}[\d]{3}[a-zA-Z]{1}$/, // 9 XX-999-X
    /^[a-zA-Z]{1}[\d]{3}[a-zA-Z]{2}$/, // 10 X-999-XX
    /^[a-zA-Z]{3}[\d]{2}[a-zA-Z]{1}$/, // 11 XXX-99-X
    /^[a-zA-Z]{1}[\d]{2}[a-zA-Z]{3}$/, // 12 X-99-XXX
    /^[\d]{1}[a-zA-Z]{2}[\d]{3}$/, // 13 9-XX-999
    /^[\d]{3}[a-zA-Z]{2}[\d]{1}$/, // 14 999-XX-9
  ];

  function parseSideCode(value) {
    return value.replace(/-/g, "").toUpperCase();
  }

  function getSideCode(value) {
    // Find index based on RegExp sideCodes array match
    const index = sideCodes.findIndex((item) => value.match(item));
    return index + 1;
  }

  function format(value) {
    value = parseSideCode(value);

    // If value was valid, but not anymore, set invalid
    if (value.length < 6) {
      return value;
    }

    if (value.length === 6) {
      const sideCode = getSideCode(value);
      if (sideCode) {
        if (sideCode <= 6) {
          value = `${value.substr(0, 2)}-${value.substr(2, 2)}-${value.substr(
            4,
            2
          )}`;
        }
        if (sideCode === 7 || sideCode === 9) {
          value = `${value.substr(0, 2)}-${value.substr(2, 3)}-${value.substr(
            5,
            1
          )}`;
        }
        if (sideCode === 8 || sideCode === 10) {
          value = `${value.substr(0, 1)}-${value.substr(1, 3)}-${value.substr(
            4,
            2
          )}`;
        }
        if (sideCode === 11 || sideCode === 14) {
          value = `${value.substr(0, 3)}-${value.substr(3, 2)}-${value.substr(
            5,
            1
          )}`;
        }
        if (sideCode === 12 || sideCode === 13) {
          value = `${value.substr(0, 1)}-${value.substr(1, 2)}-${value.substr(
            3,
            3
          )}`;
        }
      }
      // Return value in sideCode RegExp format
      return value;
    }
    // Return unformatted value as default
    return value;
  }

  return {
    formatLicensePlate: function (value) {
      return format(value);
    },
  };
})();

export default licensePlateFormatter;
