// Dependencies
import React from "react";
import { Route, Switch } from "react-router-dom";

// Elements
import Header from "../elements/Header";
import Footer from "../elements/Footer";

// context
import { useStrapiConfig } from "../context/config";
import { BreadCrumbProvider } from "../context/breadcrumb";

// Pages
//import loadable from "@loadable/component";

// Doesn't work yet
// TODO: Loadable to integrate code splutting for SSR
// const DefaultPage = loadable(() => import("../pages/Default"), {
//   ssr: true,
// });

export default (props) => {
  const { routes, footer } = useStrapiConfig(); // get's the current routes from strapi to generate breadcrumbs with dynamically
  return (
    <div className="page-wrapper">
      <Header />
      <Switch>
        {routes.map(({ path, name, slug, Component }, key) => (
          <Route
            exact
            path={path}
            key={key}
            render={(props) => (
              <BreadCrumbProvider routes={routes} {...props}>
                <Component {...props} routes={routes} slug={slug} />
              </BreadCrumbProvider>
            )}
          />
        ))}
      </Switch>
      {footer && <Footer footer={footer} />}
    </div>
  );
};
