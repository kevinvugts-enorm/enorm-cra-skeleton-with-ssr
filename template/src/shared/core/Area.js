// Dependencies
import React from "react";

// Blocks
import BlockHero from "../blocks/Hero";
import BlockHeroHome from "../blocks/Hero/Homepage";

import CardList from "../blocks/CardList";
import CardSlider from "../blocks/CardSlider";
import BlockUSP from "../blocks/Usp";
import PartnerList from "../blocks/PartnerList";
import FeatureBlock from "../blocks/Feature";
import BlockFaq from "../blocks/Faq";
import ContactBlock from "../blocks/Contact";
import DealerLocator from "../blocks/DealerLocator";
import ThanksBlock from "../blocks/Thanks";

// elements
import Breadcrumb from "../elements/Breadcrumb";

// context
import { useBreadCrumbs } from "../context/breadcrumb";

export default (props) => {
  const { blocks } = props;

  const { breadcrumbs } = useBreadCrumbs(props.routes, props.match);

  const renderBlock = (block, index) => {
    const defaultProps = {
      key: `${block.__component}-${index}`,
      ...block,
    };

    switch (block.__component) {
      case "blocks.usp":
        return <BlockUSP {...defaultProps} />;
      case "blocks.breadcrumb":
        return <Breadcrumb {...defaultProps} crumbs={breadcrumbs} />;
      case "blocks.partner-list":
        return <PartnerList {...defaultProps} />;
      case "blocks.card-list":
        return <CardList {...defaultProps} />;
      case "blocks.card-slider":
        return <CardSlider {...defaultProps} />;
      case "blocks.hero":
        return props.slug === "home" ? (
          <BlockHeroHome {...defaultProps} />
        ) : (
          <BlockHero {...defaultProps} />
        );
      case "blocks.faq":
        return <BlockFaq {...defaultProps} />;
      case "blocks.thanks":
        return <ThanksBlock {...defaultProps} />;
      case "blocks.feature":
        return <FeatureBlock {...defaultProps} />;
      case "blocks.dealer-locator":
        return <DealerLocator {...defaultProps} />;
      case "blocks.contact":
        return <ContactBlock {...defaultProps} />;
      default:
        throw new Error(`Unhandled blocktype ${block.__component}`);
    }
  };

  // Idea
  // renderHeaderArea()
  // renderGlobalArea()
  // renderContentArea()
  // renderFooterArea()

  return blocks.map(renderBlock);
};
