// Dependencies
import React from 'react'
import { Helmet } from 'react-helmet'

export default ({ pageTitle = '', seo = [] }) => {
  const meta = seo ? seo.find(o => o.__component === 'seo.meta') || {} : {}
  const openGraph = seo
    ? seo.find(o => o.__component === 'seo.open-graph') || {}
    : {}

  const capitalizedTitle = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  return (
    <Helmet htmlAttributes={{ lang: 'nl' }}>
      <title>{capitalizedTitle(pageTitle)}</title>
      {/* Meta */}
      <meta charSet="utf-8" />
      <meta name="description" content={meta.meta_description} />
      {/* Open Graph */}
      <meta property="og:type" content="website" />
      <meta property="og:title" content={openGraph.og_title || ''} />
      <meta
        property="og:description"
        content={openGraph.og_description || ''}
      />
      <meta
        property="og:image"
        content={(openGraph.og_image && openGraph.og_image.url) || ''}
      />
    </Helmet>
  )
}
