// Dependencies
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { QueryClientProvider } from "react-query";
import { Hydrate } from "react-query/hydration";
import { loadableReady } from "@loadable/component";

// Cache
import { queryClientApp } from "../../../config/caching";

// Shared
import App from "../../shared/App";

// If we pre-render react code, we have to use hydrate method - otherwise we will get an error in the console
const root = document.getElementById("app");

const dehydratedState = window.__REACT_QUERY_STATE__;

// When Loadable Components preloaded everything, hydrate the client
loadableReady(() => {
  const renderMethod = module.hot ? ReactDOM.render : ReactDOM.hydrate;

  renderMethod(
    <QueryClientProvider client={queryClientApp}>
      <Hydrate state={dehydratedState}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </Hydrate>
    </QueryClientProvider>,
    root
  );
});

if (module.hot && process.env.NODE_ENV === "development") {
  module.hot.accept();
}
